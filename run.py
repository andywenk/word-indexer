# Copyright [2023] [Andy Wenk]

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sqlite3
import os
import mimetypes
import nltk
nltk.download('punkt')
nltk.download('stopwords')
nltk.download('all', quiet=True)
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import langdetect
import PyPDF2

# Markdown hinzufügen
mimetypes.add_type('text/markdown', '.md')

directory_path = ''

def choose_directory():
    # Benutzereingabe für das Verzeichnis
    global directory_path
    directory_path = input('Gib den Pfad zum Verzeichnis ein: ')

def start():
    # Dateien im Verzeichnis filtern und nur Textdateien und PDFs berücksichtigen
    files = os.listdir(directory_path)
    i = 0
    for file in files:
        file_path = os.path.join(directory_path, file)
        mime_type, _ = mimetypes.guess_type(file_path)
        
        if mime_type is not None:
            mime_type = mime_type.lower()
            i += 1
            if mime_type in ('text/plain', 'text/markdown', 'text/xml', 'application/xml', 'application/json', 'text/html'):
                with open(file_path, 'r', encoding='utf-8') as f:
                    document_content = f.read()
                process_document(document_content)
            elif mime_type == 'application/pdf':
                try:
                    with open(file_path, 'rb') as f:
                        pdf_reader = PyPDF2.PdfFileReader(f)
                        document_content = ""
                        for page_num in range(pdf_reader.numPages):
                            page = pdf_reader.getPage(page_num)
                            document_content += page.extractText()
                    process_document(document_content)
                except Exception as e:
                    print("  => Es gab einen Fehler beim Lesen der PDF: ", e)
                    i -= 1
                

    print(f'  => Es wurden {i} Dokumente prozessiert ... ')

# Funktion zum Verarbeiten des Dokuments basierend auf der erkannten Sprache
def process_document(document_content):
    # Erkennen der Sprache des Dokuments
    language = langdetect.detect(document_content)

    # Tokenisierung, Stoppwort-Entfernung und Lemmatisierung des Textes basierend auf der erkannten Sprache
    tokens = word_tokenize(document_content.lower())
    stop_words_de = set(stopwords.words('german'))
    stop_words_en = set(stopwords.words('english'))
    filtered_tokens_de = [word for word in tokens if word.isalpha() and word not in stop_words_de]
    filtered_tokens = [word for word in filtered_tokens_de if word.isalpha() and word not in stop_words_en]
    
    # Erstelle einen Hash, um die Häufigkeit jedes Wortes zu zählen
    word_frequency = {}
    for word in filtered_tokens:
        word_frequency[word] = word_frequency.get(word, 0) + 1

    # Speichere die Wörter und ihre Häufigkeit in der Datenbank
    for word, frequency in word_frequency.items():
        c.execute("INSERT OR REPLACE INTO word_index (word, frequency) VALUES (?, ?)", (word, frequency))
    
    # Daten schreiben
    conn.commit()

# Öffne die Datenbankverbindung
conn = sqlite3.connect('word_index.db')
c = conn.cursor()

# Erstelle eine Tabelle für den Wortindex, falls sie noch nicht existiert
c.execute('''CREATE TABLE IF NOT EXISTS word_index (
                word TEXT PRIMARY KEY,
                frequency INTEGER
            )''')

choose_directory()
start()

# Benutzerinteraktion
while True:
    print('\nWas möchtest du tun?')
    print('  1. Häufigste Wörter anzeigen')
    print('  2. Nach einem Wort suchen')
    print('  3. Datenbank leeren')
    print('  4. Pfad zu den Dokumenten eingeben und indizieren')
    print('  5. Beenden')

    choice = input('\nDeine Auswahl: ')

    if choice == '1':
        limit = int(input('\nWie viele der häufigsten Wörter möchtest du anzeigen? '))
        c.execute('SELECT word, frequency FROM word_index ORDER BY frequency DESC LIMIT ?', (limit,))
        rows = c.fetchall()

        if len(rows) == 0:
            print('  => Es gibt keine Worte im Index')
        else:
            for row in rows:
                print(f'{row[0]} - {row[1]} Mal')
    elif choice == '2':
        search_word = input('\nGib das Wort ein, nach dem du suchen möchtest: ').lower()
        c.execute('SELECT word, frequency FROM word_index WHERE word LIKE ?', (search_word,))
        rows = c.fetchall()
        for row in rows:
            print(f'{row[0]} - {row[1]} Mal')
    elif choice == '3':
        c.execute('DELETE FROM word_index')
        conn.commit()
        print('\nDatenbank geleert. Du musst erst wieder Dateien indexieren:')
        choose_directory()
        start()
    elif choice == '4':
        choose_directory()
        start()
    elif choice == '5':
        print('\n~~~ Bis zum nächsten Mal ~~~\n')
        break
    else:
        print('\nUngültige Eingabe. Bitte wähle eine gültige Option.')

# Schließe die Datenbankverbindung
conn.commit()
conn.close()
