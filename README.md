# Word Indexer

Simple Python program to index words form various file formats into a sqlite3 database. You can ask for most occured words and search for words.

## Install

You need to have Python installed. I tested it with 3.10.11

Install the requirements:

    ~ pip3 install -r requirements.txt

## Usage

    ~ chmod a+x run.py
    ~ python3 run.py

You will then see sth. like

    ~ [nltk_data] Downloading package punkt to

This will donwload the needed [NLTK data](https://pythonspot.com/nltk-stop-words/) to a directory in your `$HOME`.

In the next step you will be prompted to give a give a directory path where your files are located. As of this writing, all plain/text mime-types (including Markdown) and PDF is supported.

    ~ Gib den Pfad zum Verzeichnis ein: 

Then you can interact with the indexed words:

    ~ Was möchtest du tun?
    1. Häufigste Wörter anzeigen
    2. Nach einem Wort suchen
    3. Datenbank leeren
    4. Pfad zu den Dokumenten eingeben und indizieren
    5. Beenden

    Deine Auswahl:

## TODO

* translate to english

## License

Copyright [2023] [Andy Wenk]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

